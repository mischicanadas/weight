#!/usr/local/bin/env ruby

require 'fileutils'
require 'json'
require 'mysql2' 

folder_procesar = "/home/camara/ftp/files/"
folder_procesados = "/home/camara/ftp/files/procesados/"

def read_json(filename)
    file = File.read(filename)
    data_hash = JSON.parse(file)

    mac_address = data_hash['Source']['MacAddress']
    ip_address = data_hash['Source']['IPAddress']
    data_hash['Data'][0]['CountingInfo'].each do |a|
    
        camara = a['RuleName']
        total_in = a['In']
        total_out = a['Out']
        start_time = DateTime.parse(a['StartTime']).strftime("%Y-%m-%d %H:%M:%S")
        end_time = DateTime.parse(a['EndTime']).strftime("%Y-%m-%d %H:%M:%S")

        conn = Mysql2::Client.new(:host => 'localhost', :username => 'root', :password => 'V88Tig1', :database => 'conteos')
        rs = conn.query("SELECT * FROM camaras WHERE IP = '#{ip_address}' AND Start_Time = '#{start_time}'")
        if rs.size == 0
            conn.query("INSERT INTO camaras(Camara,Archivo,MAC,IP,Total_In,Total_Out,Start_Time,End_Time) VALUES('#{camara}','#{filename}','#{mac_address}','#{ip_address}',#{total_in},#{total_out},'#{start_time}','#{end_time}')")
        end
        conn.close

        puts "Camara: #{camara} MAC: #{mac_address} IP: #{ip_address} IN: #{total_in} OUT: #{total_out} Start Time: #{start_time} End Time: #{end_time}"
    end
end

Dir.open(folder_procesar).each do |filename|
    next if File.directory? filename

    if File.extname(filename) == ".json"
       
       begin
        read_json(folder_procesar+filename)
        FileUtils.mv(folder_procesar+filename, folder_procesados)
      
       rescue
        FileUtils.mv(folder_procesar+filename, "#{folder_procesados}/#{File.basename(filename,".json")}.err")
       end

    end

end