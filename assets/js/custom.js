$(document).ready(function(){

    //$(".content").load("dashboard.html");
    
    $(".sidebar-wrapper ul li").on("click",function(){
        
        //Clean sidebar selection
        $(".sidebar-wrapper ul li").each(function(){
            
            $(this).removeClass('active');
        });

        $(this).addClass('active');
        
        switch($(this).children().children('p').text())
        {
            case "Dashboard": $(".content").load("dashboard.html");break;
            case "Perfil de usuario": $(".content").load("perfil.html");break;
            case "Calafias": $(".content").load("calafias.html");break;
            case "Notificaciones": $(".content").load("notificaciones.html");break;
        }

    });
});