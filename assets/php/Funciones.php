<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
    if(isset($_POST['Action']))
    {
        $Result = "";
        
        $Action = $_POST["Action"];
        
        switch($Action)
        {
            case "get_conteo": $Result = get_conteo();break;
            case "get_conteo_registros": $Result = get_conteo_registros();break;
            case "get_conteo_registros_fechas": $Result = get_conteo_registros_fechas();break;
            case "get_conteo_registros_acumulado": $Result = get_conteo_registros_acumulado();break;
            case "get_total_calafias": $Result = get_total_calafias();break;
            case "get_total_entradas": $Result = get_total_entradas();break;
            case "get_total_salidas": $Result = get_total_salidas();break;
        }
        
        echo $Result;
    }

    function get_total_entradas()
    {
        $FechaInicio = $_POST["FechaInicio"];
        $FechaFin = $_POST["FechaFin"];
        $HoraInicio = $_POST["HoraInicio"];
        $HoraFin = $_POST["HoraFin"];

        $Row = array();
        $Query = "select sum(Total_In)as Total from vista_registros where Start_Time BETWEEN '".$FechaInicio." ".$HoraInicio.":00:00' AND '".$FechaFin." ".$HoraFin.":59:59'";
        $result = Ejecuta_Query($Query);
        
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);

    }

    function get_total_salidas()
    {
        $FechaInicio = $_POST["FechaInicio"];
        $FechaFin = $_POST["FechaFin"];
        $HoraInicio = $_POST["HoraInicio"];
        $HoraFin = $_POST["HoraFin"];

        $Row = array();
        $Query = "select sum(Total_Out)as Total from vista_registros where Start_Time BETWEEN '".$FechaInicio." ".$HoraInicio.":00:00' AND '".$FechaFin." ".$HoraFin.":59:59'";
        $result = Ejecuta_Query($Query);
        
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);

    }

    function get_total_calafias()
    {
        $Row = array();
        $Query = "select count(distinct calafia)as Total from calafias";
        $result = Ejecuta_Query($Query);
        
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);
    }

    function get_conteo()
    {
        $FechaInicio = $_POST["FechaInicio"];
        $FechaFin = $_POST["FechaFin"];
        
        $Row = array();
        $Query = "SELECT HOUR(Start_Time)AS Hour,SUM(Total_In)AS Total_In,SUM(Total_Out)AS Total_Out FROM vista_registros WHERE (Total_In > 0 OR Total_Out > 0) AND DATE(Start_Time) BETWEEN '".$FechaInicio."' AND '".$FechaFin."' GROUP BY 1";

        $result = Ejecuta_Query($Query);
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);
    }

    function get_conteo_registros()
    {
        $Row = array();
        $Query = "select Calafia as Ubicacion, Tipo as Camara,Total_In,Total_Out,Start_Time,End_Time from vista_registros where (Total_In > 0 or Total_Out > 0) AND DATE(Start_Time) = DATE(NOW()) ORDER BY Start_Time DESC";

        $result = Ejecuta_Query($Query);
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);
    }

    function get_conteo_registros_fechas()
    {
        $FechaInicio = $_POST["FechaInicio"];
        $FechaFin = $_POST["FechaFin"];
        $HoraInicio = $_POST["HoraInicio"];
        $HoraFin = $_POST["HoraFin"];

        $Row = array();
        $Query = "select Calafia as Ubicacion, Tipo as Camara,Total_In,Total_Out,Start_Time,End_Time from vista_registros where (Total_In > 0 or Total_Out > 0) AND Start_Time BETWEEN '".$FechaInicio." ".$HoraInicio.":00:00' AND '".$FechaFin." ".$HoraFin.":59:59' ORDER BY Start_Time DESC LIMIT 100";

        $result = Ejecuta_Query($Query);
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);
    }

    function get_conteo_registros_acumulado()
    {
        $Row = array();
        $Query = "select Calafia as Ubicacion,IP, Tipo as Camara,SUM(Total_In)AS Total_In,SUM(Total_Out)AS Total_Out from vista_registros GROUP BY Calafia,Tipo,IP";
        $result = Ejecuta_Query($Query);
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);
    }

    function run($Command)
    {
        $res = exec($Command);
        return $res;
    }

    function Ejecuta_Query ($Query)
    {
        include("ConnDB.php");
        
        //Abre una conexion a MySQL server
        $mysqli = new mysqli($ConnDB["Servidor"],$ConnDB["Usuario"],$ConnDB["Password"],$ConnDB["DB"]);

        //Arrojo cualquier error tipo connection error
        if ($mysqli->connect_error) {
            die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
        }
        $result = $mysqli->query($Query);
        //Cierro la conexion 
        $mysqli->close();
        
        return $result;
    }
?>